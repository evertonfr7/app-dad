const multer = require("multer");
const path = require("path");
const crypto = require("crypto");
const fs = require("fs");
const aws = require("aws-sdk");
const multerS3 = require("multer-s3");

const storageTypes = {
    local: multer.diskStorage({
        destination: async (req, file, cb) => {
            if (!fs.existsSync(path.resolve(__dirname, '..', '..', `files/${req.params.code}/${req.url.split('/')[2]}/`))){
                await fs.promises.mkdir(path.resolve(__dirname, '..', '..', `files/${req.params.code}/${req.url.split('/')[2]}/`), { recursive: true })
            }
            cb(null, path.resolve(__dirname, '..', '..', `files/${req.params.code}/${req.url.split('/')[2]}/`));
        },
        filename: (req, file, cb) => {
            const ext = path.extname(file.originalname);
            const name = path.basename(file.originalname, ext);
            name.replace(' ', '-');
            
            cb(null, `${req.params.code}-${Date.now()}${ext}`);
        },
    }),
  s3: multerS3({
    s3: new aws.S3(),
    bucket: "rodetransp",
    contentType: multerS3.AUTO_CONTENT_TYPE,
    acl: "public-read",
    key: (req, file, cb) => {
      crypto.randomBytes(16, (err, hash) => {
        if (err) cb(err);
        const ext = path.extname(file.originalname);
        const name = path.basename(file.originalname, ext);
        name.replace(' ', '-');
        const fileName = `${req.params.code}-${Date.now()}${ext}`;
        cb(null, fileName);
      });
    }
  })
};

module.exports = {
  dest: path.resolve(__dirname, "..", "..", "tmp", "uploads"),
  storage: storageTypes[process.env.STORAGE_TYPE],
  limits: {
    fileSize: 35 * 1024 * 1024
  },
  fileFilter: (req, file, cb) => {
    const allowedMimes = [
      "image/jpeg",
      "image/pjpeg",
      "image/png",
      "image/gif",
    ];

    if (allowedMimes.includes(file.mimetype)) {
      cb(null, true);
    } else {
      cb(new Error("Invalid file type."));
    }
  }
};