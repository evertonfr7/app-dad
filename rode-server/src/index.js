require('dotenv').config();
const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const routes = require('./routes');
const port = process.env.PORT || 5555;

const app = express();

app.use(cors());
app.use(bodyParser.json());
app.use(express.json());
app.use(routes);

try {
    mongoose.connect(`mongodb+srv://dbAdmin:${process.env.DBPASSWD}@cluster0.rn9hp.mongodb.net/rodetransportes?retryWrites=true&w=majority`, {
        useNewUrlParser: true,
        useUnifiedTopology: true,
    });

    app.listen(port, () => {
        console.log("Server started...");
    });
} catch (error) {
    console.log('Não foi possível se conectar com o banco de dados');
}