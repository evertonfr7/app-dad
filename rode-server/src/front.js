const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');
const path = require('path');
const port = 3333;

const app = express();

app.use(cors());
app.use(bodyParser.json());
app.use(express.json());

app.use('/', express.static(path.resolve(__dirname, '..', '..', 'frontend')));

try {
    app.listen(port, () => {
        console.log("Server started...");
    });
} catch (error) {
    console.log('Não foi possível se conectar com o banco de dados');
}