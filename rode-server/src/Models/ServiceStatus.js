const mongoose = require('mongoose');

const ServiceStatus = new mongoose.Schema({
    id: { type: Number, unique: true },
    description: String,
});

module.exports = mongoose.model('ServiceStatus', ServiceStatus);