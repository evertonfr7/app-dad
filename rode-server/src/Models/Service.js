const mongoose = require('mongoose');

const PointSchema = require('./utils/PointSchema');

const ServiceSchema = new mongoose.Schema({

    code: { type: String, unique: true, default:`#ROD${Date.now()}` },
    isDETRANService: Boolean,
    clientName: String,
    collectAddress: String,
    collectAddressCoordinates: {
        type: PointSchema,
        index: '2dsphere'
    },
    destinationAddress: String,
    destinationAddressCoordinates: {
        type: PointSchema,
        index: '2dsphere'
    },
    collaborators: [String],
    collectImgs: [String],
    deliveryImgs: [String],
    createdBy: String,
    createdAt: { type: Date, default: Date.now },
    status: { type: Number, default: 1 },
    vehicle: Object,

});

module.exports = mongoose.model('Service', ServiceSchema)