const mongoose = require('mongoose');

const UserSchema = new mongoose.Schema({
    img: { type: String, default:'default.jpg' },
    name: String,
    phoneNumber: String,
    cpf: { type: String, unique: true, null: false},
    password: String,
    permission: Number,
    isActive: { type: Boolean, default: true },
})

module.exports = mongoose.model('User', UserSchema);