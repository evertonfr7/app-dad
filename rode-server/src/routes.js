const express = require('express');
const routes = express.Router();
const multer = require('multer');
const UploadConfig = require('../config/upload');

const AutenticateController = require('./controllers/AutenticateController');
const UserController = require('./Controllers/UserController');
const ServiceController = require('./Controllers/ServiceController');
const UploadController = require('./Controllers/UploadController');
const autenticateMiddleware = require('./middlewares/auth');

routes.use(autenticateMiddleware);
routes.post('/login', AutenticateController.autenticate);

const upload = multer(UploadConfig);

routes.get('/users', UserController.index);
routes.get('/user/:cpf', UserController.show);
routes.post('/user/create', UserController.create);
routes.put('/user/edit', UserController.edit);
routes.patch('/user/deactivate/:cpf', UserController.deactivate);
routes.patch('/user/activate/:cpf', UserController.activate);
routes.delete('/user/:cpf', UserController.delete);
routes.delete('/user-all', UserController.deleteAll);

routes.get('/services', ServiceController.index);
routes.get('/service/:code', ServiceController.show);
routes.post('/service/create', ServiceController.create);
routes.put('/service/edit', ServiceController.edit);
routes.delete('/service/:code', ServiceController.delete);
routes.delete('/service-all', ServiceController.deleteAll);

routes.post('/upload/coleta/:code', upload.array('file'), UploadController.uploadCollect);
routes.post('/upload/entrega/:code', upload.array('file'), UploadController.uploadDelivery);

module.exports = routes;