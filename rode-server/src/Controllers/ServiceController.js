const Service = require('../Models/Service');

module.exports = {

    async index(req, res) {
        if (req.permission == 1 || req.permission == 2) {
            try {
                const services = await Service.find();

                return res.json(services);

            } catch (error) {
                return res.status(400).json({
                    error
                });

            }

        } else {
            return res.status(401).json({
                error: 'Você não tem autorização para visualizar serviços'
            })

        }

    },
    async show(req, res) {
        if (req.permission == 1 || req.permission == 2 || req.permission == 3) {

            const {
                code
            } = req.params;
            try {
                const service = await Service.findOne({
                    code: `#${code}`
                });
                if (!service) {
                    return res.status(404).json({
                        message: 'Serviço não encontrado.'
                    });
                } else {
                    return res.json(service);
                }

            } catch (error) {
                return res.status(400).json({
                    error
                });

            }
        } else {
            return res.status(401).json({
                error: 'Você não tem autorização para visualizar este serviço'
            })

        }
    },
    async create(req, res) {
        if (req.permission == 1 || req.permission == 2) {
            try {

                const {
                    isDETRANService,
                    clientName,
                    collectAddress,
                    collectAddressCoordinates,
                    destinationAddress,
                    destinationAddressCoordinates,
                    collaborators,
                    createdBy,
                    name,
                    phone,
                    vehicleType,
                    collectImgs,
                    deliveryImgs,
                    plate,
                    color,
                    model

                } = req.body;


                const newService = await Service.create({
                    isDETRANService,
                    clientName,
                    collectAddress,
                    collectAddressCoordinates,
                    destinationAddress,
                    destinationAddressCoordinates,
                    collaborators,
                    createdBy,
                    collectImgs,
                    deliveryImgs,
                    vehicle: {
                        owner: {
                            name: name,
                            phone: phone,
                        },
                        type: vehicleType,
                        plate: plate,
                        color: color,
                        model: model,
                    }

                });

                return res.json(newService);

            } catch (error) {
                return res.status(400).json({
                    error
                });
            }

        } else {
            return res.status(401).json({
                error: 'Você não tem autorização para criar serviços.'
            })
        }

    },
    async edit(req, res) {
        if (req.permission == 1 || req.permission == 2 || req.permission == 3) {
            try {
                const {
                    code,
                    clientName,
                    collectAddress,
                    collectAddressCoordinates,
                    destinationAddress,
                    destinationAddressCoordinates,
                    collaborators,
                    createdBy,
                    status,
                    name,
                    phone,
                    vehicleType,
                    collectImgs,
                    deliveryImgs,
                    plate,
                    color,
                    model
                } = req.body;

                const service = await Service.findOne({
                    code: code
                });
                if (!service) {
                    return res.status(404).json({
                        message: 'Serviço não encontrado com este código.'
                    });
                } else {
                    await Service.findOneAndUpdate({
                        code,
                    }, {
                        $set: {
                            clientName,
                            collectAddress,
                            collectAddressCoordinates,
                            destinationAddress,
                            destinationAddressCoordinates,
                            collaborators,
                            createdBy,
                            status,
                            collectImgs,
                            deliveryImgs,
                            vehicle: {
                                owner: {
                                    name: name,
                                    phone: phone,
                                },
                                type: vehicleType,
                                plate: plate,
                                color: color,
                                model: model,
                            }
                        }
                    }, {
                        new: true
                    }, (err, doc) => {
                        if (err) {
                            return res.status(400).json({
                                error: 'Erro no banco de dados, não foi possível editar este serviço.'
                            });
                        }
                        return res.json({
                            message: 'Serviço alterado com sucesso!',
                            doc
                        });
                    });
                }
            } catch (error) {
                return res.status(400).json({
                    error
                });
            }
        } else {
            return res.status(401).json({
                error: 'Você não tem autorização para editar serviços.'
            });
        }
    },
    async delete(req, res) {
        if (req.permission == 1 || req.permission == 2) {
            try {
                const {
                    code
                } = req.params;

                const remove = await Service.deleteOne({
                    code: `#${code}`
                });

                if (remove.deletedCount > 0) {
                    return res.json({
                        message: `Serviço deletado com sucesso!`
                    });
                } else {
                    return res.status(400).json({
                        error: `Não foi possível deletar o serviço com o código informado.`
                    });
                }
            } catch (error) {
                return res.status(400).json({
                    error
                });
            }
        } else {
            return res.status(401).json({
                error: 'Você não tem autorização para deletar serviços.'
            })
        }
    },
    async deleteAll(req, res) {
        try {

            const remove = await Service.deleteMany({});

            if (remove.deletedCount > 0) {
                return res.json({
                    message: `ok`
                });
            } else {
                return res.status(400).json({
                    error: `erro`
                });
            }
        } catch (error) {
            return res.status(400).json({
                error
            });
        }
    }

}