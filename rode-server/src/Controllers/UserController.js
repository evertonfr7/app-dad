const User = require('../Models/User');
const Service = require('../Models/Service');
const bcrypt = require('bcrypt');

module.exports = {
    async index(req, res) {
        if (req.permission == 1) {
            try {
                const users = await User.find();
                return res.json(users);
            } catch (error) {
                return res.status(400).json({
                    error
                });
            }
        } else {
            return res.status(401).json({
                error: 'Você não tem autorização para ver usuários.'
            })
        }
    },
    async show(req, res) {

        const {
            cpf
        } = req.params;

        if (req.permission == 1 || req.permission == 2 || req.permission == 3) {
            try {
                const user = await User.findOne({
                    cpf: cpf
                });
                const services = await Service.find({
                    collaborators: cpf
                });
                if (!user) {
                    return res.status(404).json({
                        message: 'Usuário não encontrado.'
                    });
                } else {
                    return res.json({
                        user: user,
                        services: services
                    });
                }
            } catch (error) {
                return res.status(400).json({
                    error
                });
            }
        } else {
            return res.status(401).json({
                error: 'Você não tem autorização para ver este usuário.'
            })
        }
    },
    async create(req, res) {
        if (req.permission == 1) {
            try {
                const {
                    name,
                    cpf,
                    phoneNumber,
                    password,
                    permission
                } = req.body;
                const user = await User.findOne({
                    cpf: cpf
                });
                if (!user) {
                    const newUser = await User.create({
                        name,
                        cpf,
                        phoneNumber,
                        password: bcrypt.hashSync(password, 10, (err, hash) => {
                            return hash;
                        }),
                        permission,
                    });

                    return res.json(newUser);
                } else {
                    return res.status(400).json({
                        error: 'Já existe um usuário cadastrado com este CPF.'
                    });
                }
            } catch (error) {
                return res.status(400).json({
                    error
                });
            }
        } else {
            return res.status(401).json({
                error: 'Você não tem autorização para criar usuários.'
            })
        }
    },
    async edit(req, res) {
        if (req.permission == 1) {
            try {
                const {
                    img,
                    name,
                    cpf,
                    phoneNumber,
                    password,
                    permission
                } = req.body;
                const user = await User.findOne({
                    cpf: cpf
                });
                if (!user) {
                    return res.status(404).json({
                        message: 'Usuário não encontrado.'
                    });
                } else {
                    await User.findOneAndUpdate({
                        cpf,
                    }, {
                        $set: {
                            img,
                            name,
                            phoneNumber,
                            password: bcrypt.hashSync(password, 10, (err, hash) => {
                                return hash;
                            }),
                            permission,
                        }
                    }, {
                        new: true
                    }, (err, doc) => {
                        if (err) {
                            return res.status(400).json({
                                error: 'Erro, não foi possível editar este usuário.'
                            });
                        }
                        return res.json({
                            message: 'Usuário alterado com sucesso!',
                            doc
                        });
                    });
                }
            } catch (error) {
                return res.status(400).json({
                    error
                });
            }
        } else {
            return res.status(401).json({
                error: 'Você não tem autorização para editar usuários.'
            })
        }
    },
    async deactivate(req, res) {        
        if (req.permission == 1) {
            try {
                const {
                    cpf
                } = req.params;
                const user = await User.findOne({
                    cpf: cpf
                });
                if (!user) {
                    return res.status(404).json({
                        message: 'Usuário não encontrado.'
                    });
                } else {
                    await User.findOneAndUpdate({
                        cpf,
                    }, {
                        $set: {
                            isActive: false,
                        }
                    }, {
                        new: true
                    }, (err, doc) => {
                        if (err) {
                            return res.status(400).json({
                                error: 'Erro, não foi possível desativar este usuário.'
                            });
                        }
                        return res.json({
                            message: 'Usuário desativado com sucesso!',
                            doc
                        });
                    });
                }
            } catch (error) {
                return res.status(400).json({
                    error
                });
            }
        } else {
            return res.status(401).json({
                error: 'Você não tem autorização para desativar usuários.'
            })
        }
    },
    async activate(req, res) {
        
        if (req.permission == 1) {
            try {
                const {
                    cpf
                } = req.params;
                const user = await User.findOne({
                    cpf: cpf
                });
                if (!user) {
                    return res.status(404).json({
                        message: 'Usuário não encontrado.'
                    });
                } else {
                    await User.findOneAndUpdate({
                        cpf,
                    }, {
                        $set: {
                            isActive: true,
                        }
                    }, {
                        new: true
                    }, (err, doc) => {
                        if (err) {
                            return res.status(400).json({
                                error: 'Erro, não foi possível ativar este usuário.'
                            });
                        }
                        return res.json({
                            message: 'Usuário ativado com sucesso!',
                            doc
                        });
                    });
                }
            } catch (error) {
                return res.status(400).json({
                    error
                });
            }
        } else {
            return res.status(401).json({
                error: 'Você não tem autorização para ativar usuários.'
            })
        }
    },
    async delete(req, res) {

        if (req.permission == 1) {
            try {
                const {
                    cpf
                } = req.params;

                const remove = await User.deleteOne({
                    cpf
                });

                if (remove.deletedCount > 0) {
                    return res.json({
                        message: `Usuário foi deletado com sucesso!`
                    });
                } else {
                    return res.status(404).json({
                        error: `Não foi possível deletar o usuário de CPF informado. CPF não encontrado.`
                    });
                }
            } catch (error) {
                return res.status(400).json({
                    error
                });
            }
        } else {
            return res.status(401).json({
                error: 'Você não tem autorização para deletar usuários.'
            })
        }
    },
    async deleteAll(req, res) {
        try {

            const remove = await User.deleteMany({});

            if (remove.deletedCount > 0) {
                return res.json({
                    message: `ok`
                });
            } else {
                return res.status(400).json({
                    error: `erro`
                });
            }
        } catch (error) {
            return res.status(400).json({
                error
            });
        }
    }
}