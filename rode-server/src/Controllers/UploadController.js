const Service = require('../Models/Service');
const fs = require("fs");
const path = require('path');

module.exports = {
    async uploadCollect (req, res){
        const { code } = req.params;
        let files = [];

        req.files.map(file => {
            if(process.env.STORAGE_TYPE === 's3'){
                files.push(file.location)
            }else{
                files.push(file.filename)
            }
        });

        const service = await Service.findOne({code: `#${code}`});

        if(!service){
            await fs.rmdirSync(path.resolve(__dirname, '..', '..', '..', `files/${code}`), { recursive: true });
            return res.status(404).json({ message: 'Serviço não encontrado.'});
        }
        else{
            let serviceCode = `#${code}`;
            await Service.findOneAndUpdate({
                code: serviceCode,
            }, {
                $set: {
                    collectImgs: files,
                }
            }, {
                new: true 
            }, (err, doc) => {
                if ( err ) {
                    return res.status(400).json({error: 'Erro no banco de dados, não foi possível salvar as fotos deste serviço.'});
                }
                return res.json({message: 'Fotos enviadas com sucesso!', doc});
            });
        }
    },
    async uploadDelivery (req, res){
        const { code } = req.params;
        let files = [];
        req.files.map(file => {
            if(process.env.STORAGE_TYPE === 's3'){
                files.push(file.location)
            }else{
                files.push(file.filename)
            }
        });

        const service = await Service.findOne({code: `#${code}`});

        if(!service){
            return res.status(404).json({message: 'Serviço não encontrado.'});
        }
        else{
            let serviceCode = `#${code}`;
            await Service.findOneAndUpdate({
                code: serviceCode,
            }, {
                $set: {
                    deliveryImgs: files,
                }
            }, {
                new: true 
            }, (err, doc) => {
                if ( err ) {
                    return res.status(400).json({error: 'Erro no banco de dados, não foi possível salvar as fotos deste serviço.'});
                }
                return res.json({message: 'Fotos enviadas com sucesso!', doc});
            });
        }
    },
}